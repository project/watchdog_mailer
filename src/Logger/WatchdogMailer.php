<?php

namespace Drupal\watchdog_mailer\Logger;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\RfcLoggerTrait;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Utility\Token;
use Psr\Log\LoggerInterface;

/**
 * {@inheritdoc}
 */
class WatchdogMailer implements LoggerInterface {

  use RfcLoggerTrait;
  use StringTranslationTrait;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * The (lazy loaded) dependency injection (DI) container.
   *
   * @var ?\Drupal\Component\DependencyInjection\ContainerInterface
   */
  protected ?ContainerInterface $container;

  /**
   * The (lazy loaded) mail manager object.
   *
   * @var ?\Drupal\Core\Mail\MailManagerInterface
   */
  protected ?MailManagerInterface $mailManager;

  /**
   * The (lazy loaded) token object.
   *
   * @var ?\Drupal\Core\Utility\Token
   */
  protected ?Token $token;

  /**
   * Constructs a new WatchdogMailer.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A config factory for retrieving required config objects.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection to run the query against.
   * @param \Drupal\Component\Datetime\TimeInterface $timeInterface
   *   The time interface service.
   * @param \Drupal\Core\State\StateInterface $stateStorage
   *   The state storage service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $dateFormatter
   *   The date formatter service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    protected LanguageManagerInterface $languageManager,
    protected Connection $connection,
    protected TimeInterface $timeInterface,
    protected StateInterface $stateStorage,
    protected DateFormatterInterface $dateFormatter,
  ) {
    $this->config = $config_factory->get('watchdog_mailer.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function log($level, $message, array $context = []): void {
    $enabled = $this->config->get('enabled');
    $recipients = $this->config->get('recipients_default');
    $subject = $this->config->get('mail_subject');
    $body = $this->config->get('mail_body');
    $notificationObjects = $this->config->get('notification_objects');
    $channel = $context['channel'];

    if ($enabled) {

      try {
        // Go through all notification objects and identify the recipients.
        foreach ($notificationObjects as $notificationObject) {
          if (!$notificationObject['enabled']) {
            // Skip! Not enabled:
            continue;
          }

          $objectChannels = $notificationObject['channels'] ?? [];
          if (!empty($notificationObject['channels'])) {
            $channelMatches = in_array($channel, $objectChannels);
            $channelsNegate = $notificationObject['channels_negate'] ?? FALSE;
            if ($channelMatches === $channelsNegate) {
              // Skip if this channel is either excluded or not included:
              continue;
            }
          }

          $objectSeverities = $notificationObject['severities'] ?? [];
          if (!empty($objectSeverities) && !in_array($level, $objectSeverities)) {
            // Skip! Severity does not match:
            continue;
          }

          $additionalRecipients = $notificationObject['recipients'] ?? [];
          $notificationObjectRecipients = array_merge($recipients, $additionalRecipients);

          // Mail a mail to all recipients collected.
          if (!empty($notificationObjectRecipients)) {
            $notificationObjectRecipients = array_unique($notificationObjectRecipients);
            $errorMessage = new FormattableMarkup($message, $context);
            $tokenValues = [
              'watchdog_mailer' => [
                  // @todo Check if the escaping is really needed:
                'channel' => Html::escape($context['channel']),
                'timestamp' => Html::escape($context['timestamp']),
                'referer' => isset($context['referer']) ? Html::escape($context['referer']) : '',
                'request_uri' => isset($context['request_uri']) ? Html::escape($context['request_uri']) : '',
                'link' => isset($context['link']) ? Html::escape($context['link']) : '',
                'uid' => Html::escape($context['uid']),
                'user_ip' => Html::escape($context['ip']),
                'message' => $errorMessage,
                  // @todo Would be nice to get the name of the log level here,
                  // but sadly class Logger doesn't seem to provide
                  // a public mapping or function for that?
                  // At least for PHP we could use one of:
                  // - https://api.drupal.org/api/drupal/core%21includes%21errors.inc/function/drupal_error_levels/10
                  // - https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Logger%21RfcLogLevel.php/function/RfcLogLevel%3A%3AgetLevels/10
                  // These are only available for PHP errors:
                'php_severity_level' => isset($context['severity_level']) ? Html::escape($context['severity_level']) : '',
                'php_backtrace' => isset($context['@backtrace_string']) ? Html::escape($context['@backtrace_string']) : '',
                'php_file' => isset($context['%file']) ? Html::escape($context['%file']) : '',
                'php_function' => isset($context['%function']) ? Html::escape($context['%function']) : '',
                'php_line' => isset($context['%line']) ? Html::escape($context['%line']) : '',
                'php_message' => isset($context['@message']) ? Html::escape($context['@message']) : '',
                'php_type' => isset($context['%type']) ? Html::escape($context['%type']) : '',
              ],
            ];
            $body = $this->token()->replace($body, $tokenValues, ['clear' => TRUE]);
            $body = Html::decodeEntities(strip_tags($body));
            $subject = $this->token()->replace($subject, $tokenValues, ['clear' => TRUE]);
            $subject = Html::decodeEntities(strip_tags($subject));
            $params = [
              'subject' => $subject,
              'body' => $body,
            ];
            $langcode = $this->languageManager->getCurrentLanguage()->getId();
            foreach ($notificationObjectRecipients as $notificationObjectRecipient) {
              // Mail the mail if the mail limit isn't reached yet.
              // If limit is reached, this sends out a limit notification mail.
              if (!$this->mailLimitReached($notificationObjectRecipient, $langcode, $tokenValues)) {
                if (!$this->mailManager()->mail(
                  'watchdog_mailer',
                  'watchdog_mailer',
                  $notificationObjectRecipient,
                  $langcode,
                  $params)
                ) {
                  throw new \Exception('There was a problem sending the message.');
                }
              }
            }
          }
        }
      }
      catch (\Exception $e) {
        // There can be no watchdog_exception to avoid a possible endless cycle.
        $this->error($e->getMessage());
      }

    }
  }

  /**
   * Helper function that is responsible for the frequency capping.
   *
   * @param string $notificationObjectRecipient
   *   The recipient of the limit mail.
   * @param string $langcode
   *   The langcode of the limit mail.
   * @param array $tokenValues
   *   The token values needed for token escaping.
   *
   * @return bool
   *   Wether or not the mail limit has been reached.
   */
  protected function mailLimitReached($notificationObjectRecipient, $langcode, array $tokenValues): bool {
    $mailLimit = $this->config->get('mail_limit');
    $mailLimitTimeFrame = $this->config->get('mail_limit_time_frame');
    $limitMailSubject = $this->config->get('limit_mail_subject');
    $limitMailBody = $this->config->get('limit_mail_body');
    if (!empty($mailLimit)) {
      // There is a mail limit that needs to be checked for.
      $sentMailCount = $this->stateStorage->get('watchdog_mailer.sentMailCount');
      $firstMailTimestamp = $this->stateStorage->get('watchdog_mailer.firstMailTimestamp');
      $currentTimestamp = $this->timeInterface->getCurrentTime();
      if ($currentTimestamp - $firstMailTimestamp > $mailLimitTimeFrame) {
        // The limit time frame has expired. The mail counter can be reset.
        $this->stateStorage->set('watchdog_mailer.sentMailCount', 1);
        $this->stateStorage->set('watchdog_mailer.firstMailTimestamp', $currentTimestamp);
      }
      elseif ($sentMailCount == $mailLimit) {
        // The limit has been reached now. Prepare and send the notification.
        $this->stateStorage->set('watchdog_mailer.sentMailCount', $sentMailCount + 1);
        $limitMailBody = $this->token()->replace($limitMailBody, $tokenValues, ['clear' => TRUE]);
        $limitMailBody = Html::decodeEntities(strip_tags($limitMailBody));
        $limitMailSubject = $this->token()->replace($limitMailSubject, $tokenValues, ['clear' => TRUE]);
        $limitMailSubject = Html::decodeEntities(strip_tags($limitMailSubject));
        $params = [
          'subject' => $limitMailSubject,
          'body' => $limitMailBody . $this->t("\n\nLimit: @mailLimit mails for every @mailLimitTimeFrame seconds.\nYou will receive Watchdog Mails again at: @resumeTime", [
            '@mailLimit' => $mailLimit,
            '@mailLimitTimeFrame' => $mailLimitTimeFrame,
            '@resumeTime' => $this->dateFormatter->format($firstMailTimestamp + $mailLimitTimeFrame),
          ]),
        ];
        if (!$this->mailManager()->mail(
          'watchdog_mailer',
          'watchdog_mailer',
          $notificationObjectRecipient,
          $langcode,
          $params)
        ) {
          throw new \Exception('There was a problem sending the message.');
        }
        // The limit notification has been sent - don't send a log mail now!
        return TRUE;
      }
      elseif ($sentMailCount < $mailLimit) {
        // The mail limit has not been reached yet - log mail can be sent.
        $this->stateStorage->set('watchdog_mailer.sentMailCount', $sentMailCount + 1);
      }
      else {
        // Mail is above limit - don't send!
        return TRUE;
      }
    }
    // The mail can be sent if we didn't return beforehand.
    return FALSE;
  }

  /**
   * Get Dependency Injection container.
   *
   * @return \Drupal\Component\DependencyInjection\ContainerInterface
   *   Current Dependency Injection container.
   */
  protected function getContainer(): ContainerInterface {
    if (!isset($this->container)) {
      // @phpstan-ignore-next-line
      $this->container = \Drupal::getContainer(); // @codingStandardsIgnoreLine This call cannot be avoided.
    }
    return $this->container;
  }

  /**
   * Get lazy-loaded Mailmanager service.
   *
   * @see https://www.drupal.org/project/watchdog_mailer/issues/3337719
   *
   * @return \Drupal\Core\Mail\MailManagerInterface
   *   MailManager service.
   */
  protected function mailManager(): MailManagerInterface {
    if (!isset($this->mailManager)) {
      $this->mailManager = $this->getContainer()->get('plugin.manager.mail');
    }
    return $this->mailManager;
  }

  /**
   * Get lazy-loaded Token service.
   *
   * @see https://www.drupal.org/project/watchdog_mailer/issues/3414001
   *
   * @return \Drupal\Core\Utility\Token
   *   Token service.
   */
  protected function token(): Token {
    if (!isset($this->token)) {
      $this->token = $this->getContainer()->get('token');
    }
    return $this->token;
  }

}
