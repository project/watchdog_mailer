<?php

declare(strict_types=1);

namespace Drupal\watchdog_mailer\Form;

use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Watchdog Mailer settings for this site.
 */
final class SettingsForm extends ConfigFormBase {

  const RFC_LOG_LEVEL_FORM_PREFIX = 'rfc';

  /**
   * The Module Handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The Renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The Email Validator service.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected $emailValidator;

  /**
   * The Database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The state storage service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $stateStorage;

  /**
   * SettingsForm constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The Module Handler service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The Renderer service.
   * @param \Drupal\Component\Utility\EmailValidatorInterface $emailValidator
   *   The Email Validator service.
   * @param \Drupal\Core\Database\Connection $database
   *   The Database service.
   * @param \Drupal\Core\State\StateInterface $stateStorage
   *   The state storage service.
   */
  public function __construct(ModuleHandlerInterface $moduleHandler, RendererInterface $renderer, EmailValidatorInterface $emailValidator, Connection $database, StateInterface $stateStorage) {
    $this->moduleHandler = $moduleHandler;
    $this->renderer = $renderer;
    $this->emailValidator = $emailValidator;
    $this->database = $database;
    $this->stateStorage = $stateStorage;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'),
      $container->get('renderer'),
      $container->get('email.validator'),
      $container->get('database'),
      $container->get('state'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'watchdog_mailer_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['watchdog_mailer.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('watchdog_mailer.settings');

    // Provide token browser if the token module is installed:
    $tokenModuleInstalled = $this->moduleHandler->moduleExists('token');
    if ($tokenModuleInstalled) {
      $token_tree = [
        '#theme' => 'token_tree_link',
        '#token_types' => ['watchdog_mailer'],
      ];
      $rendered_token_tree = $this->renderer->render($token_tree);
    }
    else {
      $rendered_token_tree = $this->t('To use tokens, install the <a href="https://www.drupal.org/project/token" target="_blank">Token</a> module.');
    }

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Watchdog Mailer'),
      '#description' => $this->t('Check to enable the mailer function and reveal additional configuration.'),
      '#default_value' => $config->get('enabled'),
    ];

    $form['mail_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Mail Settings'),
      '#states' => [
        'visible' => [
          ':input[name="enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    // @todo Change #type to "email" (multiple) once this core issue is fixed: https://www.drupal.org/project/drupal/issues/3296190
    $form['mail_settings']['recipients_default'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default recipient email address(es)'),
      '#description' => $this->t('Enter the email address(es) that should receive copies of all watchdog entries independent of the created notification objects, separated with a comma.<br><i>Example: "webmaster@example.com" or "sales@example.com, support@example.com"</i>'),
      '#default_value' => $this->arrayToEmails($config->get('recipients_default')),
    ];

    $form['mail_settings']['mail_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mail Subject'),
      '#description' => $this->t(
          'The subject of the mail to send. <em>You may also use tokens. Empty tokens will be cleared. @browse_tokens_link</em>', [
            '@browse_tokens_link' => $rendered_token_tree,
          ]
      ),
      '#maxlength' => 512,
      '#default_value' => $config->get('mail_subject'),
    ];

    $form['mail_settings']['mail_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Mail Body'),
      '#description' => $this->t(
          'The body of the mail to send. <em>You may also use tokens. Empty tokens will be cleared. @browse_tokens_link</em>', [
            '@browse_tokens_link' => $rendered_token_tree,
          ]
      ),
      '#default_value' => $config->get('mail_body'),
    ];

    $form['mail_settings']['frequency_capping'] = [
      '#type' => 'details',
      '#title' => $this->t('Frequency Capping (Mass email protection)'),
      '#open' => FALSE,
    ];

    $form['mail_settings']['frequency_capping']['mail_limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Mail Limit'),
      '#min' => 0,
      '#description' => $this->t('Maximum amount of mails to send within the given time frame. If capped, a mail notifying you of the limit will be sent. Set to 0 to disable the limit feature.'),
      '#default_value' => $config->get('mail_limit'),
    ];

    $form['mail_settings']['frequency_capping']['mail_limit_time_frame'] = [
      '#type' => 'number',
      '#title' => $this->t('Mail Limit Time Frame'),
      '#min' => 1,
      '#description' => $this->t('The time frame in seconds that the limit is bound to. If the limit is reached within this time period, no more mails will be sent and you will get notified.'),
      '#default_value' => $config->get('mail_limit_time_frame'),
      '#states' => [
        'invisible' => [
          ':input[name="mail_limit"]' => ['value' => 0],
        ],
      ],
    ];

    $form['mail_settings']['frequency_capping']['limit_mail_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mail Limit Notification Mail Subject'),
      '#description' => $this->t(
          'The subject of the mail to send when the mail limit is reached. <em>You may also use tokens. Empty tokens will be cleared. @browse_tokens_link</em>', [
            '@browse_tokens_link' => $rendered_token_tree,
          ]
      ),
      '#maxlength' => 512,
      '#default_value' => $config->get('limit_mail_subject'),
      '#states' => [
        'invisible' => [
          ':input[name="mail_limit"]' => ['value' => 0],
        ],
      ],
    ];

    $form['mail_settings']['frequency_capping']['limit_mail_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Mail Limit Notification Mail Body'),
      '#description' => $this->t(
          'The body of the mail to send when the mail limit is reached. <em>You may also use tokens. Empty tokens will be cleared. @browse_tokens_link</em><br>Further information about the limit and time frame will be appended to the mail body automatically.', [
            '@browse_tokens_link' => $rendered_token_tree,
          ]
      ),
      '#default_value' => $config->get('limit_mail_body'),
      '#states' => [
        'invisible' => [
          ':input[name="mail_limit"]' => ['value' => 0],
        ],
      ],
    ];

    $form['notification_objects'] = [
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#title' => $this->t('Notification Types'),
      '#states' => [
        'visible' => [
          ':input[name="enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $notificationObjects = $config->get('notification_objects');
    foreach ($notificationObjects as $key => $notificationObject) {
      $form['notification_objects'][$key] = [
        '#type' => 'fieldset',
      ];
      $form['notification_objects'][$key]['enabled'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enabled'),
        '#description' => $this->t('Check to enable this notification type and receive mails from it.'),
        '#default_value' => $notificationObject['enabled'],
      ];
      $form['notification_objects'][$key]['channels'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Channels'),
        '#description' => $this->t(
          'The channels this notification type listens for, use a single line for each channel. All if none given.<br><b>Known channels:</b> @present_log_channels', [
            '@present_log_channels' => $this->getPresentLogChannels(),
          ]
        ),
        '#default_value' => $this->arrayToChannels($notificationObject['channels']),
        '#states' => [
          'visible' => [
            ':input[name="notification_objects[' . $key . '][enabled]"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['notification_objects'][$key]['channels_negate'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Exclude Channels'),
        '#description' => $this->t('If checked, will send notifications for all channels <strong>except</strong> the ones provided above.'),
        '#default_value' => $notificationObject['channels_negate'],
        '#states' => [
          'visible' => [
            ':input[name="notification_objects[' . $key . '][enabled]"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['notification_objects'][$key]['severities'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Severities'),
        '#options' => $this->getRfcLogLevelOptions(),
        '#description' => $this->t('The severities that this notification type should listen for. All if none given.'),
        '#default_value' => $this->prefixRfcLogLevels($notificationObject['severities'], TRUE),
        '#states' => [
          'visible' => [
            ':input[name="notification_objects[' . $key . '][enabled]"]' => ['checked' => TRUE],
          ],
        ],
      ];
      // @todo Change #type to "email" (multiple) once this core issue is fixed: https://www.drupal.org/project/drupal/issues/3296190
      $form['notification_objects'][$key]['recipients'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Additional recipient address(es)'),
        '#description' => $this->t('Email address(es) that should specifically receive mails from this notification type (in addition to default recipients), separated with a comma.<br><i>Example: "webmaster@example.com" or "sales@example.com, support@example.com"</i>'),
        '#default_value' => $this->arrayToEmails($notificationObject['recipients']),
        '#states' => [
          'visible' => [
            ':input[name="notification_objects[' . $key . '][enabled]"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['notification_objects'][$key]['remove'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Remove'),
        '#description' => $this->t('Removes this notification type.'),
        '#default_value' => FALSE,
        '#states' => [
          'visible' => [
            ':input[name="notification_objects[' . $key . '][enabled]"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['notification_objects'][$key]['remove_notice'] = [
        '#type' => 'item',
        '#title' => $this->t('This notification type will be deleted after saving!'),
        '#states' => [
          'visible' => [
            ':input[name="notification_objects[' . $key . '][remove]"]' => ['checked' => TRUE],
            ':input[name="notification_objects[' . $key . '][enabled]"]' => ['checked' => TRUE],
          ],
        ],
      ];
    }

    $form['add_notification_object'] = [
      '#type' => 'submit',
      '#value' => 'Add Notification Type',
      '#name' => 'add_notification_object',
      '#states' => [
        'visible' => [
          ':input[name="enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    if (!$this->checkRecipientsValid($form_state->getValue('recipients_default'))) {
      $form_state->setErrorByName('recipients_default', $this->t('Invalid email address(es) given.'));
    }
    $notificationObjects = $form_state->getValue('notification_objects') ?? [];
    foreach ($notificationObjects as $key => $notificationObject) {
      if (!$this->checkRecipientsValid($notificationObject['recipients'])) {
        $form_state->setErrorByName('notification_objects][' . $key . '][recipients', $this->t('Invalid email address(es) given.'));
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * Helper function to validate recipients fields.
   *
   * @param string $recipients
   *   The recipients field data to validate.
   *
   * @return bool
   *   Wether or not the given recipients data is valid.
   */
  public function checkRecipientsValid(string $recipients): bool {
    if (!empty($recipients)) {
      $recipients = explode(',', $recipients);
      foreach ($recipients as $recipient) {
        $recipient = trim($recipient);
        if (empty($recipient) || !$this->emailValidator->isValid($recipient)) {
          // Return early if any recipient is invalid.
          return FALSE;
        }
      }
    }
    // All recipients are valid.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $notificationObjectsConfig = [];
    $notificationObjects = $form_state->getValue('notification_objects') ?? [];
    foreach ($notificationObjects as $notificationObject) {
      if ($notificationObject['remove']) {
        // Don't save the object if it is marked to be removed.
        continue;
      }
      $notificationObjectsConfig[] = [
        'enabled' => $notificationObject['enabled'],
        'channels' => $this->channelsToArray($notificationObject['channels']),
        'channels_negate' => $notificationObject['channels_negate'],
        'severities' => $this->unprefixRfcLogLevels($notificationObject['severities'], TRUE),
        'recipients' => $notificationObject['recipients'] === '' ? [] : $this->emailsToArray($notificationObject['recipients']),
      ];
    }

    // Append a new Notification Object if the add button was pressed.
    if ($form_state->getTriggeringElement()['#name'] === 'add_notification_object') {
      $notificationObjectsConfig[] = [
        'enabled' => FALSE,
        'channels' => [],
        'channels_negate' => FALSE,
        'severities' => [],
        'recipients' => [],
      ];
    }

    // Reset frequency capping counter if setting has changed:
    if ($this->config('watchdog_mailer.settings')->get('mail_limit') != $form_state->getValue('mail_limit')) {
      $this->stateStorage->setMultiple([
        'watchdog_mailer.sentMailCount' => 0,
        'watchdog_mailer.firstMailTimestamp' => 0,
      ]);
    }

    $this->config('watchdog_mailer.settings')
      ->set('enabled', $form_state->getValue('enabled'))
      ->set('recipients_default', $form_state->getValue('recipients_default') === '' ? [] : $this->emailsToArray($form_state->getValue('recipients_default')))
      ->set('mail_subject', $form_state->getValue('mail_subject'))
      ->set('mail_body', $form_state->getValue('mail_body'))
      ->set('mail_limit', $form_state->getValue('mail_limit'))
      ->set('mail_limit_time_frame', $form_state->getValue('mail_limit_time_frame'))
      ->set('limit_mail_subject', $form_state->getValue('limit_mail_subject'))
      ->set('limit_mail_body', $form_state->getValue('limit_mail_body'))
      ->set('notification_objects', $notificationObjectsConfig)
      ->save();
    parent::submitForm($form, $form_state);

  }

  /**
   * Returns RfcLogLevel::getLevels() with keys prefixed with 'rfc_'.
   *
   * This is needed because Drupal FAPI #options don't allow "0" keys.
   * (Which would be equal to none selected).
   *
   * @return array
   *   The prefixed FAPI #options array.
   */
  protected function getRfcLogLevelOptions(): array {
    return $this->prefixRfcLogLevels(RfcLogLevel::getLevels());
  }

  /**
   * Helper method to add the form prefix from the $rfcLogLevels.
   *
   * This is needed because Drupal FAPI #options don't allow "0" keys.
   * (Which would be equal to none selected).
   *
   * @param array $rfcLogLevels
   *   The log levels to prefix.
   * @param bool $prefixValues
   *   Whether or not to also prefix the values.
   *
   * @return array
   *   The prefixed log levels array.
   */
  protected function prefixRfcLogLevels(array $rfcLogLevels, $prefixValues = FALSE): array {
    $result = [];
    foreach ($rfcLogLevels as $key => $level) {
      if ($prefixValues) {
        $level = self::RFC_LOG_LEVEL_FORM_PREFIX . $level;
      }
      $result[self::RFC_LOG_LEVEL_FORM_PREFIX . $key] = $level;
    }
    return $result;
  }

  /**
   * Helper method to remove the form prefix from the $rfcLogLevels.
   *
   * This is needed because Drupal FAPI #options don't allow "0" keys.
   * (Which would be equal to none selected).
   *
   * @param array $rfcLogLevels
   *   The log levels to be unprefixed.
   * @param bool $filterEmpty
   *   Whether or not to filter out empty keys.
   *
   * @return array
   *   The unprefixed log levels array.
   */
  protected function unprefixRfcLogLevels(array $rfcLogLevels, $filterEmpty = FALSE): array {
    $result = [];
    foreach ($rfcLogLevels as $key => $level) {
      if (!$filterEmpty || !empty($level)) {
        $rfcLevelAsInt = str_replace(self::RFC_LOG_LEVEL_FORM_PREFIX, '', $key);
        $result[$rfcLevelAsInt] = $rfcLevelAsInt;
      }
    }
    return $result;
  }

  /**
   * Turns a string containing multiple comma separated emails into an array.
   *
   * Note that this function does not validate its input string and is
   * separate to the validate_form() function.
   *
   * @param string $emailString
   *   The email string to turn into an array.
   *
   * @return array
   *   The array of email strings.
   */
  protected function emailsToArray($emailString): array {
    $result = [];
    $recipients = explode(',', $emailString);
    foreach ($recipients as $email) {
      $email = trim($email);
      // Only add non-empty email addresses:
      if (!empty($email)) {
        $result[] = $email;
      }
    }
    return $result;
  }

  /**
   * Turns an array containing email strings back into a comma separated string.
   *
   * Note that this function does not validate its input array and is
   * separate to the validate_form() function.
   *
   * @param array $emailArray
   *   The email array to turn into an string.
   *
   * @return string
   *   The string of emails separated by comma and space.
   */
  protected function arrayToEmails(array $emailArray): string {
    return implode(',', $emailArray);
  }

  /**
   * Turns a string containing multiple channels into an array.
   *
   * Note that this function does not validate its input string and is
   * separate to the validate_form() function.
   *
   * @param string $channelString
   *   The channels string to turn into an array.
   *
   * @return array
   *   The array of channel strings.
   */
  protected function channelsToArray($channelString): array {
    $result = [];
    $channels = explode("\n", $channelString);
    foreach ($channels as $channel) {
      $channel = trim($channel);
      // Only add non-empty channels:
      if (!empty($channel)) {
        $result[] = $channel;
      }
    }
    return $result;
  }

  /**
   * Turns an array containing channel strings back into a string.
   *
   * Note that this function does not validate its input array and is
   * separate to the validate_form() function.
   *
   * @param array $channelArray
   *   The channel array to turn into an string.
   *
   * @return string
   *   The string of channels separated by newlines.
   */
  protected function arrayToChannels(array $channelArray): string {
    return implode("\n", $channelArray);
  }

  /**
   * Returns an array of all previously used logger channels.
   */
  protected function getPresentLogChannels(): string {
    $channelsArray = [
      'access denied',
      'content',
      'cron',
      'form',
      'locale',
      'php',
      'system',
      'user',
    ];
    // If the Watchdog Module is enabled,
    // determine previously used log channels and add them to the list:
    if ($this->moduleHandler->moduleExists('watchdog')) {
      $databaseLogChannels = $this->database->query('SELECT DISTINCT([type]) FROM {watchdog} ORDER BY [type]')
        ->fetchAllKeyed(0, 0);
      $channelsArray = array_unique(array_merge($channelsArray, $databaseLogChannels));
      sort($channelsArray);
    }
    return implode(', ', $channelsArray);
  }

}
