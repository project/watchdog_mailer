<?php

declare(strict_types=1);

namespace Drupal\Tests\watchdog_mailer\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Tests the #states functionality of the Watchdog Mailer module settings form.
 *
 * @group watchdog_mailer
 */
final class SettingsFormStatesTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['watchdog_mailer'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * A user with admin permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->adminUser = $this->drupalCreateUser(['administer watchdog_mailer']);
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Test callback.
   */
  public function testSettingsFormStates(): void {
    $this->drupalGet('/admin/config/development/watchdog_mailer');
    $page = $this->getSession()->getPage();
    $enabled = $page->findField('enabled');
    $watchdog_mailer_settings = $page->find('css', '#edit-mail-settings');
    $recipients_default = $page->findField('recipients_default');
    $mail_subject = $page->findField('mail_subject');
    $mail_body = $page->findField('mail_body');
    $mail_limit = $page->findField('mail_limit');
    $mail_limit_time_frame = $page->findField('mail_limit_time_frame');
    $limit_mail_subject = $page->findField('limit_mail_subject');
    $limit_mail_body = $page->findField('limit_mail_body');
    $notification_object_enabled = $page->findField('notification_objects[0][enabled]');
    $channels = $page->findField('notification_objects[0][channels]');
    $channels_negate = $page->findField('notification_objects[0][channels_negate]');
    $severities = $page->find('css', '#edit-notification-objects-0-severities--wrapper');
    $recipients = $page->findField('notification_objects[0][recipients]');
    // Test initial condition.
    $this->assertTrue($enabled->isVisible());
    $this->assertTrue($watchdog_mailer_settings->isVisible());
    $this->assertTrue($recipients_default->isVisible());
    $this->assertTrue($mail_subject->isVisible());
    $this->assertTrue($mail_body->isVisible());
    $this->assertTrue($enabled->isVisible());
    $this->assertFalse($mail_limit->isVisible());
    $this->assertFalse($mail_limit_time_frame->isVisible());
    $this->assertFalse($limit_mail_subject->isVisible());
    $this->assertFalse($limit_mail_body->isVisible());
    $this->assertTrue($notification_object_enabled->isVisible());
    $this->assertTrue($channels->isVisible());
    $this->assertTrue($channels_negate->isVisible());
    $this->assertTrue($severities->isVisible());
    $this->assertTrue($recipients->isVisible());
    // Disable watchdog mailer functionality.
    $enabled->uncheck();
    $this->assertTrue($enabled->isVisible());
    $this->assertFalse($watchdog_mailer_settings->isVisible());
    $this->assertFalse($recipients_default->isVisible());
    $this->assertFalse($mail_subject->isVisible());
    $this->assertFalse($mail_body->isVisible());
    $this->assertFalse($mail_limit->isVisible());
    $this->assertFalse($mail_limit_time_frame->isVisible());
    $this->assertFalse($limit_mail_subject->isVisible());
    $this->assertFalse($limit_mail_body->isVisible());
    $this->assertFalse($notification_object_enabled->isVisible());
    $this->assertFalse($channels->isVisible());
    $this->assertFalse($channels_negate->isVisible());
    $this->assertFalse($severities->isVisible());
    $this->assertFalse($recipients->isVisible());
  }

}
