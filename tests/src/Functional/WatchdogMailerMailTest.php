<?php

namespace Drupal\Tests\watchdog_mailer\Functional;

/**
 * This class provides methods specifically for testing something.
 *
 * @group watchdog_mailer
 */
class WatchdogMailerMailTest extends WatchdogMailerTestBase {

  /**
   * Tests if a mail is sent to the email address specified in the settings.
   */
  public function testMailSingle() {
    $this->drupalGet('/admin/config/development/watchdog_mailer');
    $edit = [
      'enabled' => TRUE,
      'recipients_default' => 'foo@bar.baz',
      'mail_subject' => 'mail test subject',
      'mail_body' => 'Mail test body.',
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->rebuildContainer();
    // Produce a log entry:
    \Drupal::service('logger.factory')
      ->get('php')
      ->error('Error in the database log: php');
    $mails = $this->getMails(['module' => 'watchdog_mailer']);
    $this->assertCount(1, $mails);
    $this->assertMailString('subject', $edit['mail_subject'], 1);
    $this->assertMailString('body', $edit['mail_body'], 1);
  }

  /**
   * Test email sending with multiple destination email-address.
   */
  public function testMailMultiple() {
    $this->drupalGet('/admin/config/development/watchdog_mailer');
    $edit = [
      'enabled' => TRUE,
      'recipients_default' => 'foo@bar.baz,example@example.com',
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->rebuildContainer();
    // Produce a log entry:
    \Drupal::service('logger.factory')
      ->get('php')
      ->error('Error in the database log: php');
    $mails = $this->getMails(['module' => 'watchdog_mailer']);
    $this->assertCount(2, $mails);
    $this->assertMailString('to', 'foo@bar.baz', 2);
    $this->assertMailString('to', 'example@example.com', 2);
  }

  /**
   * Test channel and severity filtering.
   */
  public function testChannelSeverityFiltering() {
    $this->drupalGet('/admin/config/development/watchdog_mailer');
    $edit = [
      'notification_objects[0][enabled]' => TRUE,
      'notification_objects[0][recipients]' => 'example@example.com',
      'notification_objects[0][channels]' => "foo\nbaz",
      'notification_objects[0][severities][rfc3]' => TRUE,
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->rebuildContainer();
    // Produce a notice log entry:
    foreach (['foo', 'bar', 'baz', 'php'] as $channel) {
      \Drupal::service('logger.factory')
        ->get($channel)
        ->notice("Error in the database log: $channel");
    }
    // Test mails with notice log entry.
    $mails = $this->getMails(['module' => 'watchdog_mailer']);
    $this->assertCount(0, $mails);
    // Produce an error log entry:
    foreach (['foo', 'bar', 'baz', 'php'] as $channel) {
      \Drupal::service('logger.factory')
        ->get($channel)
        ->error("Error in the database log: $channel");
    }
    // Test mails with error log entry. Only Foo and Baz should appear.
    $mails = $this->getMails(['module' => 'watchdog_mailer']);
    $this->assertCount(2, $mails);
    $this->assertMailString('subject', 'Error in the database log: foo', 2);
    $this->assertMailString('subject', 'Error in the database log: baz', 2);
    // Create a second notification object and set it to php only.
    $this->submitForm([], 'Add Notification Type');
    $this->rebuildContainer();
    $edit = [
      'notification_objects[1][enabled]' => TRUE,
      'notification_objects[1][recipients]' => 'example2@example2.com',
      'notification_objects[1][channels]' => 'php',
      'notification_objects[1][severities][rfc3]' => TRUE,
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->rebuildContainer();
    // Produce an error log entry:
    foreach (['foo', 'bar', 'baz', 'php'] as $channel) {
      \Drupal::service('logger.factory')
        ->get($channel)
        ->error("Error in the database log: $channel");
    }
    // There should be three additional mails now since php is logged as well.
    $mails = $this->getMails(['module' => 'watchdog_mailer']);
    $this->assertCount(5, $mails);
    $this->assertMailString('subject', 'Error in the database log: foo', 3);
    $this->assertMailString('subject', 'Error in the database log: baz', 3);
    $this->assertMailString('subject', 'Error in the database log: php', 3);
  }

  /**
   * Test channel negating.
   */
  public function testChannelsNegate() {
    // Negate the channels via channels_negate.
    $this->drupalGet('/admin/config/development/watchdog_mailer');
    $edit = [
      'notification_objects[0][enabled]' => TRUE,
      'notification_objects[0][recipients]' => 'example@example.com',
      'notification_objects[0][channels]' => "foo\nbaz",
      'notification_objects[0][channels_negate]' => TRUE,
      'notification_objects[0][severities][rfc3]' => TRUE,
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->rebuildContainer();
    // Produce an error log entry:
    foreach (['foo', 'bar', 'baz', 'php'] as $channel) {
      \Drupal::service('logger.factory')
        ->get($channel)
        ->error("Error in the database log: $channel");
    }
    // Test mails with error log entry. Only Bar and PHP should appear.
    $mails = $this->getMails(['module' => 'watchdog_mailer']);
    $this->assertCount(2, $mails);
    $this->assertMailString('subject', 'Error in the database log: bar', 2);
    $this->assertMailString('subject', 'Error in the database log: php', 2);
    // Create a second notification object and set it to all except for php.
    $this->submitForm([], 'Add Notification Type');
    $this->rebuildContainer();
    $edit = [
      'notification_objects[1][enabled]' => TRUE,
      'notification_objects[1][recipients]' => 'example2@example2.com',
      'notification_objects[1][channels]' => 'php',
      'notification_objects[1][channels_negate]' => TRUE,
      'notification_objects[1][severities][rfc3]' => TRUE,
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->rebuildContainer();
    // Produce an error log entry:
    foreach (['foo', 'bar', 'baz', 'php'] as $channel) {
      \Drupal::service('logger.factory')
        ->get($channel)
        ->error("Error in the database log: $channel");
    }
    // There should be five additional mails now since both objects are logging.
    $mails = $this->getMails(['module' => 'watchdog_mailer']);
    $this->assertCount(7, $mails);
    $this->assertMailString('subject', 'Error in the database log: foo', 5);
    $this->assertMailString('subject', 'Error in the database log: bar', 5);
    $this->assertMailString('subject', 'Error in the database log: baz', 5);
    $this->assertMailString('subject', 'Error in the database log: php', 5);
  }

}
