<?php

declare(strict_types=1);

namespace Drupal\Tests\watchdog_mailer\Functional;

use Drupal\Tests\BrowserTestBase;
use Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException;

/**
 * ServiceCircularReferenceException testing.
 *
 * @group watchdog_mailer
 */
final class ServiceCircularReferenceExceptionTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test callback.
   */
  public function testServiceReferences(): void {
    try {
      $this->container->get('module_installer')->install(['layout_builder']);
      $this->container->get('module_installer')->install(['watchdog_mailer']);
      $this->rebuildAll();
    }
    catch (ServiceCircularReferenceException $e) {
      $this->fail('ServiceCircularReferenceException detected.');
    }
  }

}
