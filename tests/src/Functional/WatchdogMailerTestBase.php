<?php

namespace Drupal\Tests\watchdog_mailer\Functional;

use Drupal\Core\Test\AssertMailTrait;
use Drupal\Tests\BrowserTestBase;

/**
 * Base class for watchdog_mailer functional testing.
 *
 * @group watchdog_mailer
 */
abstract class WatchdogMailerTestBase extends BrowserTestBase {

  use AssertMailTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'watchdog_mailer',
  ];

  /**
   * A user with authenticated permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $regularUser;

  /**
   * A user with admin permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->adminUser = $this->drupalCreateUser(['administer watchdog_mailer']);
    $this->regularUser = $this->drupalCreateUser();
    $this->drupalLogin($this->adminUser);
  }

}
