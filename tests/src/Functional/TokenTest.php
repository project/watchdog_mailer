<?php

namespace Drupal\Tests\watchdog_mailer\Functional;

/**
 * Test token functionality.
 *
 * @group watchdog_mailer
 */
class TokenTest extends WatchdogMailerTestBase {

  /**
   * {@inheritdoc}
   */

  /**
   * Test token browser.
   */
  public function testSettingsFormWithToken() {
    // Check settings form without token module.
    $this->drupalGet('/admin/config/development/watchdog_mailer');
    $this->assertSession()
      ->linkByHrefExists('https://www.drupal.org/project/token');
    // Check settings form with token module.
    \Drupal::service('module_installer')->install(['token']);
    $this->drupalGet('/admin/config/development/watchdog_mailer');
    $this->assertSession()
      ->linkByHrefExists('/token/tree');
  }

  /**
   * Test mail sending with tokens.
   */
  public function testMailWithToken() {
    \Drupal::configFactory()
      ->getEditable('system.site')
      ->set('name', 'Foobar Site')
      ->save();
    $this->drupalGet('/admin/config/development/watchdog_mailer');
    $edit = [
      'enabled' => TRUE,
      'recipients_default' => 'foo@bar.baz',
      'mail_subject' => 'mail test subject: [site:name]',
      'mail_body' => 'Mail test body: [current-user:account-name] [watchdog_mailer:message] [watchdog_mailer:channel]',
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->rebuildContainer();
    // Produce a log entry:
    \Drupal::service('logger.factory')
      ->get('php')
      ->error('logger_message');
    $mails = $this->getMails(['module' => 'watchdog_mailer']);
    $this->assertCount(1, $mails);
    $this->assertMailString('subject', 'mail test subject: Foobar Site', 1);
    $this->assertMailString('body', "Mail test body: {$this->adminUser->name->value} logger_message php", 1);
  }

}
