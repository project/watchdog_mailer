<?php

namespace Drupal\Tests\watchdog_mailer\Functional;

use Drupal\Tests\system\Functional\Module\GenericModuleTestBase;

/**
 * Generic module test for watchdog_mailer.
 *
 * @group watchdog_mailer
 */
class GenericTest extends GenericModuleTestBase {}
