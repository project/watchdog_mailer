<?php

namespace Drupal\Tests\watchdog_mailer\Functional;

/**
 * This class provides methods specifically for testing something.
 *
 * @group watchdog_mailer
 */
class WatchdogMailerLimitTest extends WatchdogMailerTestBase {

  /**
   * Tests the frequency capping feature.
   */
  public function testMailLimit() {
    $this->drupalGet('/admin/config/development/watchdog_mailer');
    $edit = [
      'enabled' => TRUE,
      'recipients_default' => 'foo@bar.baz',
      'mail_subject' => 'mail test subject',
      'mail_body' => 'mail test body',
      'mail_limit' => 3,
      'mail_limit_time_frame' => 5,
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->rebuildContainer();
    // Produce 5 log entries:
    for ($i = 1; $i <= 5; $i++) {
      \Drupal::service('logger.factory')
        ->get('php')
        ->error('Error in the database log: php');
    }
    // Expect 4 emails, one of them being the limit mail.
    $mails = $this->getMails(['module' => 'watchdog_mailer']);
    $this->assertCount(4, $mails);
    $this->assertMailString('subject', 'Mail limit reached', 4);
    // Wait for 6 seconds to make sure the time frame has expired.
    sleep(6);
    // Log another 5 errors.
    // This time there should be 8 mails incl. 2 limit mails.
    for ($i = 1; $i <= 5; $i++) {
      \Drupal::service('logger.factory')
        ->get('php')
        ->error('Error in the database log: php');
    }
    $mails = $this->getMails(['module' => 'watchdog_mailer']);
    $this->assertCount(8, $mails);
    $this->assertMailString('subject', 'Mail limit reached', 4);
  }

  /**
   * Tests that the sent mail count is reset after changing the settings.
   */
  public function testMailLimitReset() {
    $this->drupalGet('/admin/config/development/watchdog_mailer');
    $edit = [
      'enabled' => TRUE,
      'recipients_default' => 'foo@bar.baz',
      'mail_subject' => 'mail test subject',
      'mail_body' => 'mail test body',
      'mail_limit' => 3,
      'mail_limit_time_frame' => 100000,
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->rebuildContainer();
    // Produce 5 log entries:
    for ($i = 1; $i <= 5; $i++) {
      \Drupal::service('logger.factory')
        ->get('php')
        ->error('Error in the database log: php');
    }
    // Expect 4 emails, one of them being the limit mail.
    $mails = $this->getMails(['module' => 'watchdog_mailer']);
    $this->assertCount(4, $mails);
    $this->assertMailString('subject', 'Mail limit reached', 4);
    // Go back to the settings page and change mail_limit.
    // The sent mail count should have been reset now.
    $this->drupalGet('/admin/config/development/watchdog_mailer');
    $edit = [
      'mail_limit' => 2,
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->rebuildContainer();
    // Log another 5 errors.
    // This time there should be 7 mails incl. 2 limit mails.
    for ($i = 1; $i <= 5; $i++) {
      \Drupal::service('logger.factory')
        ->get('php')
        ->error('Error in the database log: php');
    }
    $mails = $this->getMails(['module' => 'watchdog_mailer']);
    $this->assertCount(7, $mails);
    $this->assertMailString('subject', 'Mail limit reached', 4);
  }

}
