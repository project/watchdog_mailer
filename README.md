# Watchdog Mailer

The Watchdog Mailer module provides the functionality of sending mails to
specified email addresses when a PHP error of any kind is logged on the page
that it is enabled on.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/watchdog_mailer).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/watchdog_mailer).

## Requirements

This module requires no modules outside of Drupal Core.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

1. Go to *Administration > Configuration > Development > Logging and errors*
   and check *"Enable Watchdog Mailer"* in order to enable Watchdog Mailer's
   functionality.
1. Enter one or more email addresses into the *"Recipient email"* field in order
   to receive emails from the module.
1. Use the options below that to further customize the behavior of the module.

## Maintainers

- Zoltán Balogh - [Zoltán Balogh](https://www.drupal.org/u/zolt%C3%A1n-balogh)
- Julian Pustkuchen - [Anybody](https://www.drupal.org/u/anybody)
- Cameron Prince - [cameron prince](https://www.drupal.org/u/cameron-prince)
- Joshua Sedler - [Grevil](https://www.drupal.org/u/grevil)
- Kurucz István - [nevergone](https://www.drupal.org/u/nevergone)
